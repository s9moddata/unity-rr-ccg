﻿namespace Assets.Common.Scripts
{
    using System.Collections;
    using UnityEngine;
    using UnityEngine.Networking;
    using UnityEngine.UI;

    internal static class ArtLoaderUtils
    {
        public static IEnumerator SetupImage(RawImage rawImage, Texture noImagePlaceholder = null)
        {
            UnityWebRequest request = UnityWebRequestTexture.GetTexture("https://picsum.photos/200");

            yield return request.SendWebRequest();

            rawImage.texture = request.result == UnityWebRequest.Result.Success
                ? ((DownloadHandlerTexture)request.downloadHandler).texture
                : rawImage.texture = noImagePlaceholder;
        }
    }
}
