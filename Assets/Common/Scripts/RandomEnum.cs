﻿namespace Assets.Common.Scripts
{
    using System;
    using UnityEngineRandom = UnityEngine.Random;

    internal static class RandomEnum
    {
        public static T Next<T>() where T : Enum
        {
            var values = Enum.GetValues(typeof(T));
            int random = UnityEngineRandom.Range(0, values.Length);
            return (T)values.GetValue(random);
        }
    }
}
