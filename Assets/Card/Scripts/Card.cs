﻿namespace Assets.Card.Scripts
{
    using Assets.Card.Scripts.Constants;
    using Assets.Card.Scripts.Data;
    using Assets.Card.Scripts.Enums;
    using Assets.Common.Scripts;
    using UnityEngine;
    using UnityEngine.UI;

    internal class Card : MonoBehaviour
    {
        private const float TweenTime = 0.5f;

        [Header("Data")]
        [SerializeField]
        private CardData _cardData;

        [Header("UI")]
        [SerializeField]
        private Text _healthCounter;

        [SerializeField]
        private Text _manaCounter;

        [SerializeField]
        private Text _attackDamageCounter;

        [SerializeField]
        private Text _title;

        [SerializeField]
        private Text _description;

        [SerializeField]
        private RawImage _art;

        private int _currentHealth;
        private int _currentMana;
        private int _currentAttackDamage;

        private int Mana
        {
            set
            {
                TweenCounter(_manaCounter, _currentMana, value);
                _currentMana = value;
                _manaCounter.text = value.ToString();
            }
        }

        private int AttackDamage
        {
            set
            {
                TweenCounter(_attackDamageCounter, _currentAttackDamage, value);
                _currentAttackDamage = value;
                _attackDamageCounter.text = value.ToString();
            }
        }

        public bool RandomizeValue()
        {
            StatTypes statType = RandomEnum.Next<StatTypes>();
            int value = Random.Range(CardConstants.AttributeValueMinimum, CardConstants.AttributeValueMaximum);

            switch (statType)
            {
                case StatTypes.Health:
                    return SetHealth(value);
                case StatTypes.Mana:
                    Mana = value;
                    break;
                case StatTypes.AttackDamage:
                    AttackDamage = value;
                    break;
            }

            return false;
        }

        public void Reposition(Vector3 newPosition, float zRotation)
        {
            LeanTween.cancel(gameObject);

            LeanTween.value(gameObject, transform.position, newPosition, TweenTime).setOnUpdateVector3(position =>
            {
                transform.position = position;
            });

            LeanTween.rotateZ(gameObject, zRotation, TweenTime);
        }

        private void Start()
        {
            SetupStatsValues();
            SetupUI();
        }

        private void SetupStatsValues()
        {
            SetHealth(_cardData.Health);
            Mana = _cardData.Mana;
            AttackDamage = _cardData.AttackDamage;
        }

        private void SetupUI()
        {
            _title.text = _cardData.Title;
            _description.text = _cardData.Description;
            StartCoroutine(ArtLoaderUtils.SetupImage(_art, _cardData.NoImagePlaceholder));
        }

        private bool SetHealth(int health)
        {
            bool isDying = health < 1;

            TweenCounter(_healthCounter, _currentHealth, health);
            _currentHealth = health;
            _healthCounter.text = health.ToString();

            if (isDying)
            {
                gameObject.SetActive(false);
            }

            return isDying;
        }

        private void TweenCounter(Text counter, int currentValue, int newValue)
        {
            LeanTween.cancel(counter.gameObject);
            LeanTween.value(counter.gameObject, currentValue, newValue, TweenTime).setOnUpdate(v =>
            {
                counter.text = ((int)v).ToString();
            });
        }
    }
}
