﻿namespace Assets.Card.Scripts
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;

    internal class CardManager : MonoBehaviour
    {
        [Header("Card")]
        [SerializeField]
        private Card _card;

        [Header("Spawn Settings")]
        [SerializeField]
        private int _cardSpawnCountMinimum = 4;

        [SerializeField]
        private int _cardSpawnCountMaximum = 6;

        [SerializeField]
        [Range(-180f, 180f)]
        private float _arcTwist = -10f;

        [SerializeField]
        private float _cardSpawnXOffset = 120f;

        [SerializeField]
        private float _cardSpawnYOffset = -20f;

        [Header("Randomization")]
        [SerializeField]
        private float _randomizationDelayTime = 0.75f;

        private List<Card> _spawnedCards;
        private WaitForSeconds _randomizationDelay;
        private bool _isRandomizing;

        public void RandomizeCards()
        {
            if (!_isRandomizing)
            {
                StartCoroutine(Randomize());
            }
        }

        private IEnumerator Randomize()
        {
            _isRandomizing = true;
            bool requireRepositioning = false;

            for (int i = _spawnedCards.Count - 1; i > -1; i--)
            {
                if (_spawnedCards[i].RandomizeValue())
                {
                    requireRepositioning = true;
                    _spawnedCards.RemoveAt(i);
                }

                yield return _randomizationDelay;
            }

            if (requireRepositioning)
            {
                RepositionSpawnedCards();
            }

            _isRandomizing = false;
        }

        private void Start()
        {
            _randomizationDelay = new WaitForSeconds(_randomizationDelayTime);
            SpawnCards();
        }

        private void SpawnCards()
        {
            int cardsCount = Random.Range(_cardSpawnCountMinimum, _cardSpawnCountMaximum);
            _spawnedCards = new List<Card>(cardsCount);

            for (int i = 0; i < cardsCount; i++)
            {
                Card card = Instantiate(_card);
                card.transform.SetParent(transform);
                card.transform.SetAsFirstSibling();

                _spawnedCards.Add(card);
            }

            RepositionSpawnedCards();
        }

        private void RepositionSpawnedCards()
        {
            if (_spawnedCards == null || _spawnedCards.Count == 0)
            {
                return;
            }

            Vector3 screenCenterPosition = new Vector3(Screen.width * 0.5f, Screen.height * 0.4f, 0f);
            int centerIndex = _spawnedCards.Count / 2;

            for (int i = 0; i < _spawnedCards.Count; i++)
            {
                float xPosition = 0f;
                float yNudge = 0f;
                float zRotation = 0f;

                if (i != centerIndex)
                {
                    int centerOffset = centerIndex - i;
                    xPosition = centerOffset * _cardSpawnXOffset;
                    yNudge = Mathf.Abs(centerOffset) * _cardSpawnYOffset;
                    zRotation = centerOffset * _arcTwist;
                }

                Vector3 repositionPosition = screenCenterPosition;
                repositionPosition.x += xPosition;
                repositionPosition.y += yNudge;

                _spawnedCards[i].Reposition(repositionPosition, zRotation);
            }
        }
    }
}
