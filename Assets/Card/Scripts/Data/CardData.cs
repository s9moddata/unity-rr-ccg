﻿namespace Assets.Card.Scripts.Data
{
    using Assets.Card.Scripts.Constants;
    using UnityEngine;

    [CreateAssetMenu(fileName = "CardData", menuName = "ScriptableObjects/CardData")]
    internal class CardData : ScriptableObject
    {
        [Header("Stats")]
        [SerializeField]
        [Range(CardConstants.AttributeValueMinimum, CardConstants.AttributeValueMaximum)]
        private int _health;

        [SerializeField]
        [Range(CardConstants.AttributeValueMinimum, CardConstants.AttributeValueMaximum)]
        private int _mana;

        [SerializeField]
        [Range(CardConstants.AttributeValueMinimum, CardConstants.AttributeValueMaximum)]
        private int _attackDamage;

        [Header("Text")]
        [SerializeField]
        private string _title = "Title";

        [SerializeField]
        private string _description = "Description";

        [Header("Art")]
        [SerializeField]
        private Texture _noImagePlaceholder;

        public int Health => _health;

        public int Mana => _mana;

        public int AttackDamage => _attackDamage;

        public string Title => _title;

        public string Description => _description;

        public Texture NoImagePlaceholder => _noImagePlaceholder;
    }
}
