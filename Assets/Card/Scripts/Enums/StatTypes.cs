﻿namespace Assets.Card.Scripts.Enums
{
    internal enum StatTypes
    {
        Health = 0,
        Mana = 1,
        AttackDamage = 2,
    }
}
