﻿namespace Assets.Card.Scripts.Constants
{
    internal static class CardConstants
    {
        public const int AttributeValueMinimum = -2;
        public const int AttributeValueMaximum = 9;
    }
}
